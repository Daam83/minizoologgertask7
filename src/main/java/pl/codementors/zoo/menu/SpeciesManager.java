package pl.codementors.zoo.menu;

import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class SpeciesManager extends BaseManager<Species, SpeciesDAO> {

    public SpeciesManager() {
        dao = new SpeciesDAO();
    }

    @Override
    protected Species parseNew(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        return new Species(name);
    }

    @Override
    protected void copyId(Species from, Species to) {
        to.setId(from.getId());
    }

}
